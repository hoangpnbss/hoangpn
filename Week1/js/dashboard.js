// TABLE
const tBody = document.getElementById('table-body')

// FORM
const addForm = document.getElementById('add-form')

// INPUT
const nameDev = document.getElementById('nameDev')
const ip = document.getElementById('ip')
const mac = document.getElementById('mac')
const power = document.getElementById('power')

// TOGGLE SIDEBAR
const btnShowNav = document.getElementById('nav-toggle')
const sitebar = document.getElementById('sidebar')

const dashboard = document.getElementById('dashboard-container')

// SHOW SIDEBAR
btnShowNav.addEventListener('click', function () {
  sidebar.classList.add('show')
})

// HIDE SIDE BAR
dashboard.addEventListener('click', function hideSidebar() {
  sidebar.classList.remove('show')
})

let dummyData = [
  {
    nameDev: 'Tivi',
    mac: '11:11:11:11:11',
    ip: '192.168.1.1',
    date: '11-16-2021',
    power: '50',
  },
  {
    nameDev: 'Washer',
    mac: '11:11:11:11:11',
    ip: '192.168.1.1',
    date: '11-15-2021',
    power: '60',
  },
  {
    nameDev: 'Refrigenerator',
    mac: '11:11:11:11:11',
    ip: '192.168.1.1',
    date: '11-18-2021',
    power: '80',
  },

  {
    nameDev: 'Laptop',
    mac: '11:11:11:11:11',
    ip: '192.168.1.1',
    date: '11-18-2021',
    power: '40',
  },
]

function renderTable() {
  // CLEAR OLD DATA
  while (tBody.hasChildNodes()) {
    tBody.removeChild(tBody.firstChild)
  }
  dummyData.forEach((dev) => {
    const tRow = document.createElement('tr')
    for (key in dev) {
      const td = document.createElement('td')
      td.textContent = dev[key]
      tRow.append(td)
    }
    tBody.appendChild(tRow)
  })
  // CACULATE TOTAL POWER CONSUMPTION
  const tRow = document.createElement('tr')
  const total = dummyData
    .map((dev) => dev.power)
    .reduce((prev, curr) => Number(prev) + Number(curr), 0)
  tRow.innerHTML = `<td>Total</td><td></td><td></td><td></td><td>${total}</td>`
  tBody.appendChild(tRow)
}

window.addEventListener(onload, renderTable())

// ADD DATA
addForm.addEventListener('submit', function handleAdd(e) {
  e.preventDefault()

  // VALIDATE
  if (nameDev.value.trim() === '' || ip.value.trim() === '') {
    const message = document.createElement('p')
    message.classList.add('message')
    message.textContent = 'Name và IP là bắt buộc!'
    addForm.parentNode.insertBefore(message, addForm)
    setTimeout(() => {
      //   clear message
      addForm.parentNode.removeChild(message)
    }, 3000)
    //   CLEAR INPUT
    nameDev.value = ''
    ip.value = ''
    mac.value = ''
    power.value = ''
    return
  }

  // HANDLE TIMEZONE
  const date = new Date()
  let intlDateObj = new Intl.DateTimeFormat('en-US', {
    timeZone: 'Asia/Ho_Chi_Minh',
  })

  let vnTime = intlDateObj.format(date)
  console.log('VN date: ' + vnTime)

  const newDev = {
    nameDev: nameDev.value,
    mac: mac.value,
    ip: ip.value,
    date: `${vnTime.toString()}`,
    power: power.value,
  }

  // ADD NEW DEVICE TO CHART
  addData(myChart, newDev.nameDev, Number(newDev.power))

  // ADD NEW DEVICE TO DATA
  const newData = [...dummyData, newDev]
  dummyData = newData

  // RERENDER TABLE AFTER ADD DATA
  renderTable()

  //   CLEAR INPUT
  nameDev.value = ''
  ip.value = ''
  mac.value = ''
  power.value = ''
})

// CHART
const labels = dummyData.map((dev) => dev.nameDev)
const dataChart = dummyData.map((dev) => Number(dev.power))

const data = {
  labels: labels,
  datasets: [
    {
      label: 'Device Power Comsumption',
      data: dataChart,
      backgroundColor: [
        'rgba(255, 99, 132, 0.5)',
        'rgba(54, 162, 235, 0.5)',
        'rgba(255, 206, 86, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(153, 102, 255, 0.5)',
        'rgba(255, 159, 64, 0.5)',
      ],
      hoverOffset: 4,
      responsive: true,
    },
  ],
}

const config = {
  type: 'doughnut',
  data: data,
}

// ADD DATA TO CHART
function addData(chart, label, data) {
  chart.data.labels.push(label)
  chart.data.datasets[0].data.push(data)
  chart.update()
}

const myChart = new Chart(document.getElementById('myChart'), config)
