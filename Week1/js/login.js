// // get container form
// const containerForm = document.getElementById('login').parentNode()
// get form log
const loginForm = document.getElementById('login-form')
// get input field
const username = document.getElementById('username')
const password = document.getElementById('password')
// valid username password
const validUser = 'john'
const validPass = '1234'

loginForm.addEventListener('submit', function handleSubmit(e) {
  e.preventDefault()

  if (username.value === validUser && password.value === validPass) {
    //   redirect to dashboard
    console.log('ting ting')
    window.location.href = 'dashboard.html'
    return
  }
  //   message
  const message = document.createElement('p')
  message.classList.add('message')
  message.textContent = 'Tài khoản hoặc mật khẩu không hợp lệ!'
  loginForm.parentNode.insertBefore(message, loginForm)
  setTimeout(() => {
    //   clear message
    loginForm.parentNode.removeChild(message)
  }, 3000)
  //    clear input
  username.value = ''
  password.value = ''
})
