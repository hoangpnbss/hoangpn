// GLOBAL DATA
let globalData = [
  {
    id: 1,
    name: 'TV',
    action: 'Turn On',
    date: '2468',
  },
  {
    id: 2,
    name: 'Washer',
    action: 'Turn On',
    date: '2468',
  },
  {
    id: 3,
    name: 'Selling Fan',
    action: 'Turn Off',
    date: '2468',
  },
  {
    id: 4,
    name: 'Cleaner',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 5,
    name: 'Selling Fan',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 6,
    name: 'Washer',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 7,
    name: 'Cleaner',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 8,
    name: 'Random',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 9,
    name: 'Random',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 10,
    name: 'TV',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 11,
    name: 'Cleaner',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 12,
    name: 'Random',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 13,
    name: 'TV',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 14,
    name: 'Selling Fan',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 15,
    name: 'Random',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 16,
    name: 'Random',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 17,
    name: 'Random',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 18,
    name: 'Random',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 19,
    name: 'Random',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 20,
    name: 'Random',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 21,
    name: 'TV',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 22,
    name: 'TV',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 23,
    name: 'TV',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 24,
    name: 'TV',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 25,
    name: 'TV',
    action: 'Sleep',
    date: '2468',
  },
  {
    id: 26,
    name: 'TV',
    action: 'Sleep',
    date: '2468',
  },
]

let dummyData = [...globalData]

// tbody table
const tBody = document.getElementById('log-table-body')

// Form search
const searchForm = document.getElementById('search-form')
// Input search box
const searchBox = document.getElementById('search-box')

// PAGINATION
const pagination = document.getElementById('pagination')

// HANDLE SEARCH
searchForm.addEventListener('submit', function handleSearch(e) {
  e.preventDefault()
  if (searchBox.value === '') {
    // RESET DATA
    dummyData = [...globalData]
    renderTable(dummyData)
    return
  }
  // FILTER DATA BY NAME
  // dummyData = dummyData.filter(
  //   (d) => d.name.toLowerCase() === searchBox.value.toLowerCase()
  // )
  const keyToSearch = searchBox.value.toLowerCase()
  dummyData = dummyData.filter((d) =>
    d.name.toLowerCase().includes(keyToSearch)
  )
  renderTable(dummyData)
  searchBox.value = ''
})

// RENDER DATA TABLE
function renderTable(dummyData, page = 1) {
  paginate(dummyData.length)
  // CLEAR OLD DATA
  while (tBody.hasChildNodes()) {
    tBody.removeChild(tBody.firstChild)
  }
  const dataPage = dummyData.slice(5 * (page - 1), page * 5)
  dataPage.forEach((dev) => {
    const tRow = document.createElement('tr')
    for (key in dev) {
      const td = document.createElement('td')
      td.textContent = dev[key]
      tRow.append(td)
    }
    tBody.appendChild(tRow)
  })
}

window.addEventListener('load', renderTable(dummyData))

// PAGINATE
function paginate(numItems, limit = 5) {
  while (pagination.hasChildNodes()) {
    pagination.removeChild(pagination.firstChild)
  }
  const numPages = Math.ceil(numItems / limit)

  for (let i = 1; i <= numPages; i++) {
    const liTag = document.createElement('li')
    liTag.textContent = i
    liTag.addEventListener('click', changePage)
    pagination.appendChild(liTag)
  }
}

function changePage() {
  const page = this.textContent
  this.classList.add('page-active')
  renderTable(dummyData, page)
}
