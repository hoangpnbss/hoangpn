export const data = [
  {
    nameDev: 'Tivi',
    mac: '11:11:11:11:11',
    ip: '192.168.1.1',
    date: '11-16-2021',
    power: '50',
  },
  {
    nameDev: 'Washer',
    mac: '11:11:11:11:11',
    ip: '192.168.1.1',
    date: '11-15-2021',
    power: '60',
  },
  {
    nameDev: 'Refrigenerator',
    mac: '11:11:11:11:11',
    ip: '192.168.1.1',
    date: '11-18-2021',
    power: '80',
  },

  {
    nameDev: 'Laptop',
    mac: '11:11:11:11:11',
    ip: '192.168.1.1',
    date: '11-18-2021',
    power: '40',
  },
]
