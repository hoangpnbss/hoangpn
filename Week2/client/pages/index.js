import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { data } from '../data/data'

export default function Home() {
  console.log(data)

  return (
    <>
      <Head>
        <title>Hoang PN | Week 2</title>
        <meta name='description' content='Device Mangement App' />
        <link rel='icon' href='/favicon.ico' />
        <link
          href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css'
          rel='stylesheet'
        ></link>
      </Head>

      <div className={styles.dashboardContainer}>
        <div className={styles.tableContainer}>
          <table>
            <thead>
              <tr>
                <th>Device</th>
                <th>MAC Address</th>
                <th>IP</th>
                <th>Created Date</th>
                <th>Power Consumtion</th>
              </tr>
            </thead>
            <tbody>
              {data.map((d) => {
                return (
                  <tr key={d.nameDev}>
                    <td>{d.nameDev}</td>
                    <td>{d.mac}</td>
                    <td>{d.ip}</td>
                    <td>{d.date}</td>
                    <td>{d.power}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
        {/* <div className='chart'>
          <canvas id='myChart'></canvas>
        </div>
        <div className='add-form'>
          <form id='add-form'>
            <div className='txt-field'>
              <input type='text' id='nameDev' placeholder='Name' />
            </div>
            <div className='txt-field'>
              <input type='text' id='ip' placeholder='IP' />
            </div>
            <div className='txt-field'>
              <input type='text' id='mac' placeholder='Mac Address' />
            </div>
            <div className='txt-field'>
              <input type='text' id='power' placeholder='Power Consumtion' />
            </div>
            <button className='add-btn'>ADD DEVICE</button>
          </form>
        </div> */}
      </div>
    </>
  )
}
