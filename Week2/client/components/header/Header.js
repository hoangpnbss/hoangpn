import styles from './Header.module.css'
import Link from 'next/link'
import Image from 'next/image'
import { useState } from 'react'

export default function Header() {
  // Toggle navbar
  const [isNavVisible, setNavVisible] = useState(false)

  return (
    <>
      <header className={styles.header}>
        <div
          className={styles.mobileNavToggle}
          onClick={() => setNavVisible(!isNavVisible)}
        >
          {isNavVisible ? (
            <i className='bx bx-menu'></i>
          ) : (
            <i className='bx bx-x'></i>
          )}
        </div>
        <nav>
          <ul
            id='nav'
            className={
              isNavVisible ? styles.nav : `${styles.nav} ${styles.navActive}`
            }
          >
            <li>
              <Link href='/'>
                <a className={styles.logo}>HOANG PN</a>
              </Link>
            </li>
            <li>
              <Link href='/dashboard'>
                <a className={styles.link}>Dashboard</a>
              </Link>
            </li>
            <li>
              <Link href='/logs'>
                <a className={styles.link}>Logs</a>
              </Link>
            </li>
          </ul>
        </nav>

        <div className={styles.navUser}>
          <Image
            src='/images/perfil.jpg'
            width={35}
            height={35}
            alt='User image'
          />
          <span>John</span>
        </div>
      </header>
    </>
  )
}
